package pri._3d;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Canvas extends JPanel {
    int lenUnit;
    int centralX;
    int centralY;

    public Canvas(int lenUnit, int canvasLen) {
        this.lenUnit = lenUnit;
        this.centralX = (canvasLen - 1) / 2;
        this.centralY = centralX;
    }

    List<PxLint> lines;

    public void draw(List<Line> lines) {
        this.lines = transform(lines);
        this.repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (lines != null) {
            Graphics2D graphics = (Graphics2D) g;
            for (PxLint line : lines) {
                graphics.drawLine(line.x1, line.y1, line.x2, line.y2);
                if (line.name1 != null) {
                    graphics.drawString(line.name1, line.x1, line.y1);
                } else if (line.name2 != null) {
                    graphics.drawString(line.name2, line.x2, line.y2);
                }
            }
        }
    }

    public ArrayList<PxLint> transform(List<Line> lines) {
        ArrayList<PxLint> iLines = new ArrayList<>();
        for (Line line : lines) {
            iLines.add(line.toPxLine(centralX, centralY, lenUnit));
        }
        return iLines;
    }

}