package pri._3d;

public class Line {
    private final Point a, b;
    double x1, y1, x2, y2, z1, z2;

    public Line(Point a, Point b) {
        this.a = a;
        this.b = b;
        reset();
    }

    public void reset() {
        this.x1 = a.x;
        this.y1 = a.y;
        this.z1 = a.z;
        this.x2 = b.x;
        this.y2 = b.y;
        this.z2 = b.z;
    }

    public PxLint toPxLine(int centralX, int centralY, int unit) {
        return new PxLint(toPx(centralX, unit, x1, z1), toPx(centralY, -unit, y1, z1), a.name,
                toPx(centralX, unit, x2, z2), toPx(centralY, -unit, y2, z2), b.name);
    }

    // base on point 0,0,1
    private int toPx(int center, int unit, double len, double z) {
        return (int) ((len * unit) / (z + 1) + center);
    }
}