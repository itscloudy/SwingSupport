package pri._3d2;

import pri._3d.Canvas;
import pri._3d.Line;
import pri._3d.PxLint;
import pri.swg.Dragger;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/*
       z
       ↑/
    ---|--> y
      /|
     x

     The Z direction is the default direction
 */
public class World {
    // method to draw Entity (draw lesson from _3d package)
    static int lenUnit = 100;
    static int canvasLen = 601;

    static JFrame frame = new JFrame();
    static Canvas canvas = new Canvas(lenUnit, canvasLen);

    public World() {
        frame.setBounds(0, 0, canvasLen, canvasLen);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(1, 1));
        frame.setUndecorated(true);
        frame.add(canvas);
        Dragger.drag(frame, canvas);
    }


    public static void main(String[] args) {

    }

    private static class Canvas extends JPanel {
        int lenUnit;
        int centralX;
        int centralY;

        public Canvas(int lenUnit, int canvasLen) {
            this.lenUnit = lenUnit;
            this.centralX = (canvasLen - 1) / 2;
            this.centralY = centralX;
        }

        java.util.List<PxLint> lines;

        public void draw(java.util.List<Line> lines) {
            this.lines = transform(lines);
            this.repaint();
        }
        public ArrayList<PxLint> transform(List<Line> lines) {
            ArrayList<PxLint> iLines = new ArrayList<>();
            for (Line line : lines) {
                iLines.add(line.toPxLine(centralX, centralY, lenUnit));
            }
            return iLines;
        }

    }
}
