package pri._3d2;

/*
       z
       ↑/
    ---|--> y
      /|
     x
 */
public class Point {
    private double x;
    private double y;
    private double z;

    /**
     * x, y, z is the coordinate
     * yz, zx, xy is the radian in each datum plane
     */
    public Point(double x, double y, double z) {
        // binding coordinates
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // method to move (for base point)
    // method to rotate (for all points)

    // -- normal getter --

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    // -- normal setter --

    public void setAll(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }
}
